import React, { Component } from 'react';
import TableExample from './TableData';
import TableConfig from './TableConfig';
import HorizontalLinearStepper from './HorizontalLinearStepper';
import logo from './logo.svg';
import './App.css';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
          <MuiThemeProvider>
              <TableConfig/>
          </MuiThemeProvider>
          <MuiThemeProvider>
              <TableExample />
          </MuiThemeProvider>
          <MuiThemeProvider>
              <HorizontalLinearStepper />
          </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
