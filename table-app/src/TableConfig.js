import React , { Component } from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import DropDonw from './DropDonw';
import CheckboxExampleSimple from './CheckboxExampleSimple';


class TableConfig extends Component {

    render() {
        return (
            <Table >
                <TableHeader>
                    <TableRow>
                        <TableHeaderColumn>Колонка</TableHeaderColumn>
                        <TableHeaderColumn>Значение</TableHeaderColumn>
                        <TableHeaderColumn>Обязвтельное поле</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    <TableRow>
                        <TableRowColumn>ID</TableRowColumn>
                        <TableRowColumn>
                            <DropDonw/>
                        </TableRowColumn>
                        <TableRowColumn>
                            <CheckboxExampleSimple/>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn>Name</TableRowColumn>
                        <TableRowColumn>
                            <DropDonw/>
                        </TableRowColumn>
                        <TableRowColumn>
                            <CheckboxExampleSimple/>
                        </TableRowColumn>
                    </TableRow>
                    <TableRow>
                        <TableRowColumn>Status</TableRowColumn>
                        <TableRowColumn>
                            <DropDonw/>
                        </TableRowColumn>
                        <TableRowColumn>
                            <CheckboxExampleSimple/>
                        </TableRowColumn>
                    </TableRow>
                </TableBody>
            </Table>
        );
    }
}

export default TableConfig;